# !/usr/bin/env python
import random
from os.path import join

import nltk
import pycrfsuite
import re
from nltk import pos_tag, word_tokenize, defaultdict
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import LabelBinarizer

from swda.swda import CorpusReader

MODEL_DIR = './model/'

QUESTION_TAGS = {'qy', 'qw', 'qy^d', 'bh', 'qo', 'qh', 'fp', 'qrr', '^g', 'qw^d'}

to_rem = re.compile("[-.,'!?]")
pattern = re.compile("[a-z]+'[a-z]+")


def preprocess_pos_words(utterance_pos_words):
    result = []
    for idx, word in enumerate(utterance_pos_words):
        if str(word).startswith("'") or pattern.match(word):
            if len(result) > 0:
                result.append(result.pop() + word)
        elif not to_rem.match(word):
            result.append(word)
    return result


def load_corpus():
    labels = defaultdict(list)
    corpus = CorpusReader('swda')
    for idx, utterance in enumerate(corpus.iter_utterances(display_progress=True)):
        # if idx < 100:
        # sentence = ' '.join(utterance.pos_words())

        sentence = ' '.join(preprocess_pos_words(utterance.pos_words()))
        print(sentence)
        # print('Utterance pos words: %s' % utterance.pos_words())
        # print('Utterance pos words p: %s' % preprocess_pos_words(utterance.pos_words()))
        if str(sentence).strip() == '':
            continue
        if utterance.damsl_act_tag() in QUESTION_TAGS:
            labels['question'].append(sentence)
        else:
            labels['other'].append(sentence)

    label_set = set(labels.keys())

    train_set = []
    test_set = []
    for k, v in labels.items():
        train_idx = int(len(v) - len(v) / 5.0)
        train_set += [(sent, k) for sent in v[:train_idx]]
        test_set += [(sent, k) for sent in v[train_idx:]]

    print('Number of questions: %i' % len(labels['question']))
    print('Number of others: %i' % len(labels['other']))

    return label_set, train_set, test_set


def pos_word_feats(sent):
    text = word_tokenize(sent)
    pos_arr = pos_tag(text)
    feats = {}
    for word in pos_arr:
        feats['has({})'.format(word[0].lower())] = True
        feats['has({})'.format(word[1].lower())] = True
    return feats


def crf_feats(dataset):
    crf_ds = []
    for sent, label in dataset:
        sent_feats = []
        text = word_tokenize(sent)
        pos_arr = pos_tag(text)
        for word in pos_arr:
            sent_feats.append((word[0].lower(), word[1].lower(), label))
        crf_ds.append(sent_feats)

    return crf_ds


def word2features(token_postag_label_list, idx):
    word = token_postag_label_list[idx][0]
    postag = token_postag_label_list[idx][1]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'postag=' + postag,
        'postag[:2]=' + postag[:2],
    ]
    if idx > 0:
        preceding_word = token_postag_label_list[idx - 1][0]
        preceding_postag = token_postag_label_list[idx - 1][1]
        features.extend([
            '-1:word.lower=' + preceding_word.lower(),
            '-1:postag=' + preceding_postag,
            '-1:postag[:2]=' + preceding_postag[:2],
        ])
    else:
        features.append('BOS')

    if idx < len(token_postag_label_list) - 1:
        following_word = token_postag_label_list[idx + 1][0]
        following_postag = token_postag_label_list[idx + 1][1]
        features.extend([
            '+1:word.lower=' + following_word.lower(),
            '+1:postag=' + following_postag,
            '+1:postag[:2]=' + following_postag[:2],
        ])
    else:
        features.append('EOS')

    return features


def sent2features(token_postag_label_list):
    return [word2features(token_postag_label_list, idx) for idx in range(len(token_postag_label_list))]


def sent2labels(token_postag_label_list):
    return [label for token, postag, label in token_postag_label_list]


def sent2tokens(token_postag_label_list):
    return [token for token, postag, label in token_postag_label_list]


def train_crf(trainer):
    trainer.set_params({
        'c1': 1.0,  # coefficient for L1 penalty
        'c2': 1e-3,  # coefficient for L2 penalty
        'max_iterations': 50,  # stop earlier
        # include transitions that are possible, but not observed
        'feature.possible_transitions': True
    })

    trainer.train(join(MODEL_DIR, 'sl_{}.crfsuite'.format(kidx)))

    tagger = pycrfsuite.Tagger()
    tagger.open(join(MODEL_DIR, 'sl_{}.crfsuite'.format(kidx)))

    example_sent = total_sents[random.choice(test)]
    print(' '.join(sent2tokens(example_sent)) + '\n\n')

    print("Predicted:", ' '.join(tagger.tag(sent2features(example_sent))))
    print("Correct:  ", ' '.join(sent2labels(example_sent)))

    y_pred = []
    y_test = []
    for teidx in test:
        y_pred.append(tagger.tag(X[teidx]))
        y_test.append(y[teidx])

    acc, cr = label_classification_report(y_test, y_pred)
    print(cr)

    return acc


def label_classification_report(y_true, y_pred):
    """
    Classification report for a list of BIO-encoded sequences.
    It computes token-level metrics and discards "O" labels.

    Note that it requires scikit-learn 0.15+ (or a version from github master)
    to calculate averages properly!
    """
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(nltk.chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(nltk.chain.from_iterable(y_pred)))

    tagset = set(lb.classes_) - {'O'}
    print(tagset)
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}

    return f1_score(y_true_combined, y_pred_combined, average='weighted'), classification_report(
        y_true_combined,
        y_pred_combined,
        labels=[class_indices[cls] for cls in tagset],
        target_names=tagset,
    )


if __name__ == '__main__':
    label_set, train_set, test_set = load_corpus()

    train_featuresets = [(pos_word_feats(sample[0]), sample[1]) for sample in train_set]
    test_featuresets = [(pos_word_feats(sample[0]), sample[1]) for sample in test_set]

    total_featureset = train_featuresets + test_featuresets

    train_sents = crf_feats(train_set)
    test_sents = crf_feats(test_set)

    print("\n\nHere's how the feature set looks like...\n\n")
    print('train_set[0] %s' % str(train_set[0]))
    print('train_sents[0] %s' % str(train_sents[0]))

    total_sents = train_sents + test_sents

    X = [sent2features(s) for s in total_sents]
    y = [sent2labels(s) for s in total_sents]
    print('X[0]: %s' % X[0])
    print('y[0]: %s' % y[0])

    print("Splitting the data set in 5 folds created by preserving the \%age of samples in each class")
    # skf = StratifiedKFold(n_splits=5)
    skf = StratifiedKFold(n_splits=2)

    best_crf = 0

    # print(y)
    for kidx, (train, test) in enumerate(skf.split(X, [l[0] for l in y])):

        print("============training for fold {} ...============".format(kidx))

        trainer = pycrfsuite.Trainer(verbose=False)
        y_test = []

        for tidx in train:
            trainer.append(X[tidx], y[tidx])

        for tidx in test:
            y_test.append(total_featureset[tidx][1])

        crfacc = train_crf(trainer)

        best_crf = kidx if crfacc > best_crf else best_crf

    with open(join(MODEL_DIR, 'best.txt'), 'wb') as bm:
        bm.write('CRF\tsl_{}.crfsuite\n'.format(best_crf).encode('utf-8'))
