### Instructions
1. Clone this repository.
2. Clone swda repository: 
```https://github.com/cgpotts/swda```
3. Unzip `swda/swda.zip` and copy unzipped folder into `question_detection`.
4. Move `swda` into python's site-packages directory.
5. Create `model` dir in `question_detection` for output files.
6. Run `python main.py`.

### Dependencies
* nltk
* pycrfsuite
* swda
* scikit-learn
* re